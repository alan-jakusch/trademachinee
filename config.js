module.exports = {
    biananceBaseUrl : 'https://api.binance.com/',
    binanceApiKey : "",
    binanceSecretKey : "",
    binanceMinimumAmount: 0.001,
    
    bitinkaBaseUrl : 'https://www.bitinka.com/api/apinka/',
    bitinkaApiKey : "",
    bitinkaSecretKey : "",
    btinkaMinimumAmount : 0.001,

    huobiBaseUrl: "https://api.huobi.pro/",
    huobiApiKei : "",
    huobiSecretKey: "",
    huobiAccount: "",
    huobiMinimumAmount : 0.001,

    kucoinBaseUrl: "https://api.kucoin.com/",
    kucoinApiKei : "",
    kucoinSecretKey: "",
    kucoinPassword: "",
    kucoinMinimumAmount : 0.001,

    coinbeneMarketUrl : "http://api.coinbene.com/v1/market/",
    coinbeneTradeUrl : "http://api.coinbene.com/v1/trade/",
    coinbeneApiKey : "",
    coinbeneSecretKey: "",
    coinbeneMinimumAmount : 0.001,
    
    candle: 0.35, // em %
    minProfit: 0.15, // em %
    fee: 0.2, // em %
    minimunNanoAmount: 10,
    
    mongodb: {
      host: "localhost",
      port: "27017",
      name: "trademachine",
    },
  }
  
