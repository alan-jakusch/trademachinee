const { work } = require('./src/worker/nano-btc.v5');
const Config = require('./config');
const mongoose = require('mongoose');

const url = 'mongodb://'+Config.mongodb.host+':'+Config.mongodb.port+'/'+Config.mongodb.name;
mongoose.connect(url, { useNewUrlParser: true })
.then(async res => {
    work();
})
.catch(err => {
    console.log('erro', err);
});
