const Config = require('../../config');
const TradeModel = require('../model/trade');
const { parseNanoAmount, parseBtcAmount, trade, getProfitPercent } = require('../utils');
const {
    getCoinbeneBalance, 
    createOrderCoinbene, 
    getCoinbeneLastPrice,
} = require('../api/coinbene');
const {
    getKucoinLastPrice, 
    getKucoinBalance, 
    createOrderKucoin, 
} = require('../api/kucoin');

function reInit( time = 5000 ) {
    setTimeout(()=>{
        work();
    }, time); 
}

async function findKucoinBalance (balances) {
    const res = {
        nano: balances.free.NANO,
        btc: balances.free.BTC
    }
    return res;
}

async function findCoinbeneBalance(balances) {
    let ret = { btc: 0, nano: 0}
    for(let i in balances){
        if(balances[i].asset === "BTC"){
            ret.btc =  parseFloat(balances[i].available);
        }
        if(balances[i].asset === "NANO"){
            ret.nano =  parseFloat(balances[i].available);
        }
    }
    return ret;
}

async function postOrderCoinbene (qtd, price, orderType) {
    return new Promise(async (result, reject) => {
        const postOrder = async () => {
            let payload = {
                "type": orderType, 
                "price": parseBtcAmount(price),
                "quantity": parseNanoAmount(qtd),
                "symbol": 'nanobtc'
            }
            try {
                const res = await createOrderCoinbene(payload);
                console.log("ordem registadra em Coinbene");
                return result(res);
            } catch(e) {
                console.log("erro enviando ordem para Coinbene", e);
                console.log("tentando novamente");
                setTimeout(postOrder, 1000);
            }
        }
        console.log("enviando ordem para Coinbene");
        return await postOrder(); 
    })
}

function postOrderKucoin (qtd, price, orderType) {
    return new Promise(async (result, reject) => {
        const postOrder = async () => {
            console.log("enviando ordem para Kucoin");
            const payload = {
                symbol : "NANO/BTC", 
                side : orderType, 
                type: "limit",
                price : parseBtcAmount(price), 
                quantity : parseNanoAmount(qtd),
            }
            try {
                const res = await createOrderKucoin(payload);
                console.log("ordem registadra em kucoin");
                return result(res)
            } catch (e) {
                console.log("erro registrando ordem em Kucoin", e);
                console.log("tentando novamente");
                setTimeout(postOrder, 1000);
            }
        }
        await postOrder()
    })
}

async function sellConbeneBuyNanoKucoin ({sellAmount, xSellPrice, buyAmount, yBuyPrice}) {
   
    return Promise.all([
        postOrderKucoin(buyAmount, yBuyPrice, "buy").catch(err => { throw err }),
        postOrderCoinbene(sellAmount, xSellPrice, "sell-limit").catch(err => { throw err })
    ])
    .then(() => {

        const newTrade = {
            symbol           : 'NANOBTC',
            xName            : 'Coinbene',
            xAmount          : sellAmount,
            xPrice           : xSellPrice,
            xAction          : 'SELL',
            yName            : 'Kucoin',
            yAmount          : buyAmount,
            yPrice           : yBuyPrice,
            yAction          : 'BUY',
            profit           : buyAmount - sellAmount,
            profitPercetage  : getProfitPercent(xSellPrice - yBuyPrice, yBuyPrice) - Config.fee
        }
        const trade = new TradeModel(newTrade);
        trade.save();

        return true;

    }).catch(err => {
        console.log('erro enviando ordem', err);
        throw err;
    });
            
}

async function sellNanoKucoinBuyNanoCoinbene ({sellAmount, xSellPrice, buyAmount, yBuyPrice}) {
 
    return Promise.all([
        postOrderKucoin(sellAmount, xSellPrice, "sell").catch(err => { throw err }),
        postOrderCoinbene(buyAmount, yBuyPrice, "buy-limit").catch(err => { throw err })
    ])
    .then(() => {

        const newTrade = {
            symbol           : 'NANOBTC',
            xName            : 'Coinbene',
            xAmount          : buyAmount,
            xPrice           : yBuyPrice,
            xAction          : 'BUY',
            yName            : 'Kucoin',
            yAmount          : sellAmount,
            yPrice           : xSellPrice,
            yAction          : 'SELL',
            profit           : buyAmount - sellAmount,
            profitPercetage  : getProfitPercent(xSellPrice - yBuyPrice, yBuyPrice) - Config.fee
        }
        const trade = new TradeModel(newTrade);
        trade.save();

        return true;

    }).catch(err => {
        console.log('erro enviando ordem', err);
        throw err;
    });
}

async function work () {
    console.log("  ========================= Analizando Informações =====================================  ");

    const err = err => { throw err };
    Promise.all([
        getKucoinBalance().catch(err),
        getKucoinLastPrice('NANO/BTC').catch(err),
        getCoinbeneBalance().catch(err),
        getCoinbeneLastPrice('nanobtc').catch(err)
    ])
    .then(async (response) => {

        let kucoinBalance = await findKucoinBalance(response[0]);
        let coinbeneBalance = await findCoinbeneBalance(response[2].balance);
        const coinbenePrice = response[3];
        const kucoinPrice = response[1];

        console.log('Saldo Kucoin', kucoinBalance);
        console.log('Saldo Coinbene', coinbeneBalance);

        if(coinbeneBalance.nano > 100){
            coinbeneBalance.nano = 100;
        }
        if(kucoinBalance.nano > 100){
            kucoinBalance.nano = 100;
        }

        Promise.all([
            trade ({
                xname : 'Coinbene', 
                xBuyPrice: coinbenePrice,
                xSellPrice : coinbenePrice, 
                xBalance : coinbeneBalance.nano,
                xMinimumAmount: Config.coinbeneMinimumAmount,

                yname : 'kucoin',
                yBuyPrice : kucoinPrice,
                yBalance : kucoinBalance.btc,
                yMinimumAmount: Config.kucoinMinimumAmount,
                
                action: sellConbeneBuyNanoKucoin
            }).catch(async err => { throw err }),

            trade ({
                xname : 'kucoin', 
                xBuyPrice: kucoinPrice,
                xSellPrice : kucoinPrice,
                xBalance : kucoinBalance.nano,
                xMinimumAmount: Config.kucoinMinimumAmount,

                yname : 'Coinbene',
                yBuyPrice : coinbenePrice,
                yBalance : coinbeneBalance.btc,
                yMinimumAmount: Config.coinbeneMinimumAmount,

                action: sellNanoKucoinBuyNanoCoinbene
            }).catch(err => { throw err }),

        ]).then(data => {
            reInit()
            console.log('aguardando nova oportunidade de negocio...');
        }).catch(err => { throw err })

    }).catch(err => {
        console.log(err.message, err);
        console.log('Ops! Houve um erro. Reiniciando em breve...');
        return reInit();
    })
}

module.exports = { work };