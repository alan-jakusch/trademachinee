const Config = require('../../config');
const TradeModel = require('../model/trade');
// const { to } = require('../utils');
const {
    getBinanceOrderBook, 
    getBinanceBalance, 
    createOrderBinance, 
    // getOpenOrdersBinance, 
    searchBinancePrice
} = require('../api/binance');
const {
    getHuobiOrderBook, 
    getHuobiBalance, 
    createOrderHuobi, 
    // getOpenOrdersHuobi, 
    searchHuobiPrice
} = require('../api/huobi');

function getProfitPercent (profit, valor) {
    const x = (profit * 100) / valor;
    return x
}

function reInit( time = 20000 ) {
    setTimeout(()=>{
        work();
    }, time); 
}

async function findHuobiBalance (balances) {
    const res = {
        nano: balances.free.NANO,
        btc: balances.free.BTC
    }
    return res;
}

async function findBinanceBalance(balances) {
    let ret = { btc: 0, nano: 0}
    for(let i in balances){
        if(balances[i].asset === "BTC"){
            ret.btc =  parseFloat(balances[i].free);
        }
        if(balances[i].asset === "NANO"){
            ret.nano =  parseFloat(balances[i].free);
        }
    }
    return ret;
}

async function postOrderBinance (qtd, price, orderType) {
    const postOrder = async () => {
        const payload = {
            symbol: "NANOBTC",
            side: orderType,
            type: "LIMIT",
            timeInForce: "GTC",
            quantity: parseFloat(qtd.toString()),
            price: parseFloat(price.toString())
        }
        try {
            const res = await createOrderBinance(payload);
            console.log("ordem registadra em Binance", res);
            return res;
        } catch(e) {
            console.log("erro enviando ordem para Binance", e);
            console.log("tentando novamente");
            postOrder();
        }
    }
    console.log("enviando ordem para binance");
    return await postOrder(); 
}

function postOrderHuobi (qtd, price, orderType) {
    return new Promise(async (result, reject) => {
        const postOrder = async () => {
            console.log("enviando ordem para huobi");
            // const payload = {
            //     typeOrder : orderType, 
            //     price: parseFloat(price.toString().slice(0,10)),
            //     investement: parseFloat(qtd.toString().slice(0,4)),
            //     firstCurrency : "NANO", 
            //     secondCurrency : "BTC", 
            //     trade : 1 
            // }
            const payload = {
                symbol : "NANO/BTC", 
                side : orderType, 
                type: "limit",
                price : parseFloat(price.toString()), 
                quantity : parseFloat(qtd.toString()),
            }
            try {
                const res = await createOrderHuobi(payload);
                if(res.return === false){
                    console.log('deu erro', res);
                    console.log('payload', payload);
                    postOrder()
                }else{
                    return result(res)
                }
                
            } catch (e) {
                console.log("erro registrando ordem em huobi", e);
                console.log("tentando novamente");
                postOrder();
            }
        }
        await postOrder()
    })
}

async function sellNanoBinanceBuyNanoHuobi ({sellAmount, binanceSellPrice, buyAmount, huobiBuyPrice}) {

    return true;
   
    return Promise.all([
        postOrderHuobi(buyAmount, huobiBuyPrice, "buy").catch(err => { throw err }),
        postOrderBinance(sellAmount, binanceSellPrice, "SELL").catch(err => { throw err })
    ])
    .then(() => {

        const newTrade = {
            symbol           : 'NANOBTC',
            xName            : 'Binance',
            xAmount          : sellAmount,
            xPrice           : binanceSellPrice,
            xAction          : 'SELL',
            yName            : 'Huobi',
            yAmount          : buyAmount,
            yPrice           : huobiBuyPrice,
            yAction          : 'BUY',
            profit           : buyAmount - sellAmount,
            profitPercetage  : getProfitPercent(binanceSellPrice - huobiBuyPrice, huobiBuyPrice) - Config.fee
        }
        const trade = new TradeModel(newTrade);
        trade.save();

        return true;

    }).catch(err => {
        console.log('erro enviando ordem', err);
        throw err;
    });
            
}

async function sellNanoHuobiBuyNanoBinance ({sellAmount, huobiSellPrice, buyAmount, binanceBuyPrice}) {

    return true;
 
    return Promise.all([
        postOrderHuobi(sellAmount, huobiSellPrice, "sell").catch(err => { throw err }),
        postOrderBinance(buyAmount, binanceBuyPrice, "BUY").catch(err => { throw err })
    ])
    .then(() => {

        const newTrade = {
            symbol           : 'NANOBTC',
            xName            : 'Binance',
            xAmount          : buyAmount,
            xPrice           : binanceBuyPrice,
            xAction          : 'BUY',
            yName            : 'Huobi',
            yAmount          : sellAmount,
            yPrice           : huobiSellPrice,
            yAction          : 'SELL',
            profit           : buyAmount - sellAmount,
            profitPercetage  : getProfitPercent(huobiSellPrice - binanceBuyPrice, binanceBuyPrice) - Config.fee
        }
        const trade = new TradeModel(newTrade);
        trade.save();

        return true;

    }).catch(err => {
        console.log('erro enviando ordem', err);
        throw err;
    });
}

async function work () {
    console.log("|| ============================================================== ||");
    console.log('Buscando dados')

    Promise.all([
        getHuobiBalance().catch(err => { throw err }),
        getHuobiOrderBook('nanobtc').catch(err => { throw err }),
        getBinanceBalance(),
        getBinanceOrderBook('NANOBTC')
    ])
    .then(async (response) => {

        const huobiBalance = await findHuobiBalance(response[0]);
        const huobiOrderBook = response[1];
        const binanceBalance = await findBinanceBalance(response[2].balances);
        const binanceOrderBook = response[3];

        console.log('Saldo Huobi', huobiBalance);
        console.log('Saldo Binance', binanceBalance);

        let nanoBinance = binanceBalance.nano,
            nanoHuobi = huobiBalance.nano;
        if(nanoBinance > 100){
            nanoBinance = 100;
        }
        if(nanoHuobi > 100){
            nanoHuobi = 100;
        }

        const binanceSellPrice = await searchBinancePrice(binanceOrderBook.bids, nanoBinance);
        const binanceBuyPrice = await searchBinancePrice(binanceOrderBook.asks, nanoHuobi);
        const huobiSellPrice = await searchHuobiPrice(huobiOrderBook.bids, nanoHuobi);
        const huobiBuyPrice = await searchHuobiPrice(huobiOrderBook.asks, nanoBinance);

        if(huobiSellPrice > binanceBuyPrice) {

            console.log('Valor de venda Huobi  :', huobiSellPrice);
            console.log('Valor de compra Binance :', binanceBuyPrice);
            console.log(`Possível lucro de ${getProfitPercent(huobiSellPrice - binanceBuyPrice, binanceBuyPrice)} %`);

            if(getProfitPercent(huobiSellPrice - binanceBuyPrice, binanceBuyPrice) > Config.candle){

                let sellAmount, buyAmount;
                if(nanoHuobi * huobiSellPrice > binanceBalance.btc){
                    sellAmount = binanceBalance.btc / huobiSellPrice;
                    buyAmount = binanceBalance.btc / binanceBuyPrice;
                } else {
                    sellAmount = nanoHuobi;
                    buyAmount = (nanoHuobi * huobiSellPrice) / binanceBuyPrice;
                }

                if((sellAmount * huobiSellPrice) < Config.btinkaMinimumAmount || (buyAmount * binanceBuyPrice) < Config.binanceMinimumAmount){
                    console.log('Saldo abaixo do valor mínimo para operação');
                    return reInit();
                }
                
                sellNanoHuobiBuyNanoBinance({sellAmount, huobiSellPrice, buyAmount, binanceBuyPrice}).then( () => {

                    console.log(`vendido ${sellAmount} na Huobi por ${huobiSellPrice}, e comprado ${buyAmount} em binance por ${binanceBuyPrice}`);
                    console.log(`Lucro ${getProfitPercent(huobiSellPrice - binanceBuyPrice, binanceBuyPrice) - Config.fee} %`);
                    console.log('aguardando nova oportunidade de negocio...');
                    return reInit();

                }).catch(err => {

                    console.log('erro executando operação', err);
                    reInit();

                });
            
            } else {
                console.log(`Operação descartada : ${getProfitPercent(huobiSellPrice - binanceBuyPrice, binanceBuyPrice) - Config.fee} %`);
                return reInit();
            }

        } else if (binanceSellPrice > huobiBuyPrice) {

            console.log('Valor de venda Binance  :', binanceSellPrice);
            console.log('Valor de compra Huobi :', huobiBuyPrice);
            console.log(`Possível lucro de ${getProfitPercent(binanceSellPrice - huobiBuyPrice, huobiBuyPrice)} %`);

            if(getProfitPercent(binanceSellPrice - huobiBuyPrice, huobiBuyPrice) > Config.candle){
                const huobiBalanceBtc = huobiBalance.btc;
                let sellAmount, buyAmount;
                if(nanoBinance * binanceSellPrice > huobiBalanceBtc){
                    sellAmount = huobiBalanceBtc / binanceSellPrice;
                    buyAmount = huobiBalanceBtc / huobiBuyPrice;
                } else {
                    sellAmount = nanoBinance;
                    buyAmount = (nanoBinance * binanceSellPrice) / huobiBuyPrice;
                }

                if((sellAmount * binanceSellPrice) < Config.binanceMinimumAmount || (buyAmount * huobiBuyPrice) < Config.btinkaMinimumAmount){
                    console.log('Saldo abaixo do valor mínimo para operação');
                    return reInit();
                }

                sellNanoBinanceBuyNanoHuobi({sellAmount, binanceSellPrice, buyAmount, huobiBuyPrice}).then( () => {

                    console.log(`vendido ${sellAmount} na binance por ${binanceSellPrice}, e comprado ${buyAmount} em huobi por ${huobiBuyPrice}`);
                    console.log(`Lucro ${getProfitPercent(binanceSellPrice - huobiBuyPrice, huobiBuyPrice) - Config.fee} %`);
                    console.log('aguardando nova oportunidade de negocio...');
                    return reInit();

                }).catch(err => {

                    console.log('erro executando operação', err);
                    reInit();

                });

            } else {
                console.log(`Operação descartada : ${getProfitPercent(binanceSellPrice - huobiBuyPrice, huobiBuyPrice) - Config.fee} %`);
                return reInit();
            }

        } else {
            console.log({binanceSellPrice: binanceSellPrice, huobiBuyPrice: huobiBuyPrice, res: binanceSellPrice - huobiBuyPrice});
            console.log({huobiSellPrice: huobiSellPrice, binanceBuyPrice: binanceBuyPrice, res: huobiSellPrice - binanceBuyPrice});
            console.log('Aguardando oportunidade de negócio')
            return reInit();
        }

    }).catch(err => {
        console.log(err.message, err);
        console.log('Ops! Houve um erro. Reiniciando em breve...')
    })
}

module.exports = { work };