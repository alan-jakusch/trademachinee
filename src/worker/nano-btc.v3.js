const Config = require('../../config');
const TradeModel = require('../model/trade');
const { parseNanoAmount, parseBtcAmount, trade, getProfitPercent } = require('../utils');
const {
    getBinanceBalance, 
    createOrderBinance, 
    searchBinancePrice
} = require('../api/binance');
const {
    getBinanceLastPrice, 
} = require('../api/binance.v2');
const {
    getKucoinLastPrice, 
    getKucoinBalance, 
    createOrderKucoin, 
    searchKucoinPrice
} = require('../api/kucoin');

function reInit( time = 5000 ) {
    setTimeout(()=>{
        work();
    }, time); 
}

async function findKucoinBalance (balances) {
    const res = {
        nano: balances.free.NANO,
        btc: balances.free.BTC
    }
    return res;
}

async function findBinanceBalance(balances) {
    let ret = { btc: 0, nano: 0}
    for(let i in balances){
        if(balances[i].asset === "BTC"){
            ret.btc =  parseFloat(balances[i].free);
        }
        if(balances[i].asset === "NANO"){
            ret.nano =  parseFloat(balances[i].free);
        }
    }
    return ret;
}

async function postOrderBinance (qtd, price, orderType) {
    return new Promise(async (result, reject) => {
        const postOrder = async () => {
            const payload = {
                symbol: "NANOBTC",
                side: orderType,
                type: "LIMIT",
                timeInForce: "GTC",
                quantity: parseNanoAmount(qtd),
                price: parseBtcAmount(price)
            }
            try {
                const res = await createOrderBinance(payload);
                console.log("ordem registadra em Binance");
                return result(res);
            } catch(e) {
                console.log("erro enviando ordem para Binance", e);
                console.log("tentando novamente");
                setTimeout(postOrder, 1000);
            }
        }
        console.log("enviando ordem para binance");
        return await postOrder(); 
    })
}

function postOrderKucoin (qtd, price, orderType) {
    return new Promise(async (result, reject) => {
        const postOrder = async () => {
            console.log("enviando ordem para Kucoin");
            const payload = {
                symbol : "NANO/BTC", 
                side : orderType, 
                type: "limit",
                price : parseBtcAmount(price), 
                quantity : parseNanoAmount(qtd),
            }
            try {
                const res = await createOrderKucoin(payload);
                console.log("ordem registadra em kucoin");
                return result(res)
            } catch (e) {
                console.log("erro registrando ordem em Kucoin", e);
                console.log("tentando novamente");
                setTimeout(postOrder, 1000);
            }
        }
        await postOrder()
    })
}

async function sellNanoBinanceBuyNanoKucoin ({sellAmount, xSellPrice, buyAmount, yBuyPrice}) {
   
    return Promise.all([
        postOrderKucoin(buyAmount, yBuyPrice, "buy").catch(err => { throw err }),
        postOrderBinance(sellAmount, xSellPrice, "SELL").catch(err => { throw err })
    ])
    .then(() => {

        const newTrade = {
            symbol           : 'NANOBTC',
            xName            : 'Binance',
            xAmount          : sellAmount,
            xPrice           : xSellPrice,
            xAction          : 'SELL',
            yName            : 'Kucoin',
            yAmount          : buyAmount,
            yPrice           : yBuyPrice,
            yAction          : 'BUY',
            profit           : buyAmount - sellAmount,
            profitPercetage  : getProfitPercent(xSellPrice - yBuyPrice, yBuyPrice) - Config.fee
        }
        const trade = new TradeModel(newTrade);
        trade.save();

        return true;

    }).catch(err => {
        console.log('erro enviando ordem', err);
        throw err;
    });
            
}

async function sellNanoKucoinBuyNanoBinance ({sellAmount, xSellPrice, buyAmount, yBuyPrice}) {
 
    return Promise.all([
        postOrderKucoin(sellAmount, xSellPrice, "sell").catch(err => { throw err }),
        postOrderBinance(buyAmount, yBuyPrice, "BUY").catch(err => { throw err })
    ])
    .then(() => {

        const newTrade = {
            symbol           : 'NANOBTC',
            xName            : 'Binance',
            xAmount          : buyAmount,
            xPrice           : yBuyPrice,
            xAction          : 'BUY',
            yName            : 'Kucoin',
            yAmount          : sellAmount,
            yPrice           : xSellPrice,
            yAction          : 'SELL',
            profit           : buyAmount - sellAmount,
            profitPercetage  : getProfitPercent(xSellPrice - yBuyPrice, yBuyPrice) - Config.fee
        }
        const trade = new TradeModel(newTrade);
        trade.save();

        return true;

    }).catch(err => {
        console.log('erro enviando ordem', err);
        throw err;
    });
}

async function work () {
    console.log("  ========================= Analizando Informações =====================================  ");

    const err = err => { throw err };
    Promise.all([
        getKucoinBalance().catch(err),
        getKucoinLastPrice('NANO/BTC').catch(err),
        getBinanceBalance().catch(err),
        getBinanceLastPrice('NANO/BTC').catch(err)
    ])
    .then(async (response) => {

        let kucoinBalance = await findKucoinBalance(response[0]);
        let binanceBalance = await findBinanceBalance(response[2].balances);
        const binancePrice = response[3];
        const kucoinPrice = response[1];

        console.log('Saldo Kucoin', kucoinBalance);
        console.log('Saldo Binance', binanceBalance);

        if(binanceBalance.nano > 100){
            binanceBalance.nano = 100;
        }
        if(kucoinBalance.nano > 100){
            kucoinBalance.nano = 100;
        }

        Promise.all([
            trade ({
                xname : 'Binance', 
                xBuyPrice: binancePrice,
                xSellPrice : binancePrice, 
                xBalance : binanceBalance.nano,
                xMinimumAmount: Config.binanceMinimumAmount,

                yname : 'kucoin',
                yBuyPrice : kucoinPrice,
                yBalance : kucoinBalance.btc,
                yMinimumAmount: Config.kucoinMinimumAmount,
                
                action: sellNanoBinanceBuyNanoKucoin
            }).catch(async err => { throw err }),

            trade ({
                xname : 'kucoin', 
                xBuyPrice: kucoinPrice,
                xSellPrice : kucoinPrice,
                xBalance : kucoinBalance.nano,
                xMinimumAmount: Config.kucoinMinimumAmount,

                yname : 'Binance',
                yBuyPrice : binancePrice,
                yBalance : binanceBalance.btc,
                yMinimumAmount: Config.binanceMinimumAmount,

                action: sellNanoKucoinBuyNanoBinance
            }).catch(err => { throw err }),

        ]).then(data => {
            reInit()
            console.log('aguardando nova oportunidade de negocio...');
        }).catch(err => { throw err })

    }).catch(err => {
        console.log(err.message, err);
        console.log('Ops! Houve um erro. Reiniciando em breve...');
        return reInit();
    })
}

module.exports = { work };