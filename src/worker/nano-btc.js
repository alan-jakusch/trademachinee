const Config = require('../../config');
const TradeModel = require('../model/trade');
// const { to } = require('../utils');
const {
    getBinanceOrderBook, 
    getBinanceBalance, 
    createOrderBinance, 
    // getOpenOrdersBinance, 
    searchBinancePrice
} = require('../api/binance');
const {
    getBitinkaOrderBook, 
    getBitinkaBalance, 
    createOrderBitinka, 
    // getOpenOrdersBitinka, 
    searchBitinkaPrice
} = require('../api/bitinka');

function getProfitPercent (profit, valor) {
    const x = (profit * 100) / valor;
    return x
}

function reInit( time = 30000 ) {
    setTimeout(()=>{
        work();
    }, time); 
}

async function findBitinkaBalance (balances) {
    let ret = { btc: 0, nano: 0}
    if(typeof balances === "object" && balances.length >= 1){
        for(let i in balances){
            if(balances[i].coin === "BTC"){
                ret.btc = balances[i].balance;
            }
            if(balances[i].coin === "NANO"){
                ret.nano =  balances[i].balance;
            }
        }
    }
    return ret;
}

async function findBinanceBalance(balances) {
    let ret = { btc: 0, nano: 0}
    for(let i in balances){
        if(balances[i].asset === "BTC"){
            ret.btc =  parseFloat(balances[i].free);
        }
        if(balances[i].asset === "NANO"){
            ret.nano =  parseFloat(balances[i].free);
        }
    }
    return ret;
}

async function postOrderBinance (qtd, price, orderType) {
    const postOrder = async () => {
        const payload = {
            symbol: "NANOBTC",
            side: orderType,
            type: "LIMIT",
            timeInForce: "GTC",
            quantity: parseFloat(qtd.toString().slice(0,4)),
            price: parseFloat(price.toString().slice(0,10))
        }
        try {
            const res = await createOrderBinance(payload);
            console.log("ordem registadra em Binance", res);
            return res;
        } catch(e) {
            console.log("erro enviando ordem para Binance", e);
            console.log("tentando novamente");
            postOrder();
        }
    }
    console.log("enviando ordem para binance");
    return await postOrder(); 
}

function postOrderBitinka (qtd, price, orderType) {
    return new Promise(async (result, reject) => {
        const postOrder = async () => {
            console.log("enviando ordem para Bitinka");
            const payload = {
                typeOrder : orderType, 
                price: price.toString().slice(0,10),
                investement: qtd.toString().slice(0,4),
                firstCurrency : "NANO", 
                secondCurrency : "BTC", 
                trade : 1 
            }
            try {
                const res = await createOrderBitinka(payload);
                if(res.return === false){
                    console.log('deu erro', res);
                    console.log('payload', payload);
                    postOrder()
                }else{
                    return result(res)
                }
                
            } catch (e) {
                console.log("erro registrando ordem em bitinka", e);
                console.log("tentando novamente");
                postOrder();
            }
        }
        await postOrder()
    })
}

async function sellNanoBinanceBuyNanoBitinka ({sellAmount, binanceSellPrice, buyAmount, bitinkaBuyPrice}) {
   
    return Promise.all([
        postOrderBitinka(buyAmount, bitinkaBuyPrice, "buy").catch(err => { throw err }),
        postOrderBinance(sellAmount, binanceSellPrice, "SELL").catch(err => { throw err })
    ])
    .then(() => {

        const newTrade = {
            symbol           : 'NANOBTC',
            xName            : 'Binance',
            xAmount          : sellAmount,
            xPrice           : binanceSellPrice,
            xAction          : 'SELL',
            yName            : 'Bitinka',
            yAmount          : buyAmount,
            yPrice           : bitinkaBuyPrice,
            yAction          : 'BUY',
            profit           : buyAmount - sellAmount,
            profitPercetage  : getProfitPercent(binanceSellPrice - bitinkaBuyPrice, bitinkaBuyPrice) - Config.fee
        }
        const trade = new TradeModel(newTrade);
        trade.save();

        return true;

    }).catch(err => {
        console.log('erro enviando ordem', err);
        throw err;
    });
            
}

async function sellNanoBitinkaBuyNanoBinance ({sellAmount, bitinkaSellPrice, buyAmount, binanceBuyPrice}) {
 
    return Promise.all([
        postOrderBitinka(sellAmount, bitinkaSellPrice, "sell").catch(err => { throw err }),
        postOrderBinance(buyAmount, binanceBuyPrice, "BUY").catch(err => { throw err })
    ])
    .then(() => {

        const newTrade = {
            symbol           : 'NANOBTC',
            xName            : 'Binance',
            xAmount          : buyAmount,
            xPrice           : binanceBuyPrice,
            xAction          : 'BUY',
            yName            : 'Bitinka',
            yAmount          : sellAmount,
            yPrice           : bitinkaSellPrice,
            yAction          : 'SELL',
            profit           : buyAmount - sellAmount,
            profitPercetage  : getProfitPercent(bitinkaSellPrice - binanceBuyPrice, binanceBuyPrice) - Config.fee
        }
        const trade = new TradeModel(newTrade);
        trade.save();

        return true;

    }).catch(err => {
        console.log('erro enviando ordem', err);
        throw err;
    });
}

async function work () {
    console.log("|| ============================================================== ||");
    console.log('Buscando dados em Bitinka')
    Promise.all([
        getBitinkaBalance().catch(err => { throw err }),
        getBitinkaOrderBook('NANO_BTC').catch(err => { throw err }),
    ])
    .then(async (bitinkaData) => {
        console.log('Buscando dados em Binance')
        Promise.all([
            getBinanceBalance(),
            getBinanceOrderBook('NANOBTC')
        ])
        .then(async (binanceData) => {

            const bitinkaBalance = await findBitinkaBalance(bitinkaData[0]);
            const bitinkaOrderBook = bitinkaData[1];
            const binanceBalance = await findBinanceBalance(binanceData[0].balances);
            const binanceOrderBook = binanceData[1];
    
            console.log('Saldo Bitinka', bitinkaBalance);
            console.log('Saldo Binance', binanceBalance);
    
            let nanoBinance = binanceBalance.nano,
                nanoBitinka = bitinkaBalance.nano;
            if(nanoBinance > 100){
                nanoBinance = 100;
            }
            if(nanoBitinka > 100){
                nanoBitinka = 100;
            }
    
            const binanceSellPrice = await searchBinancePrice(binanceOrderBook.bids, nanoBinance);
            const binanceBuyPrice = await searchBinancePrice(binanceOrderBook.asks, nanoBitinka);
            const bitinkaSellPrice = await searchBitinkaPrice(bitinkaOrderBook.sales.NANO, nanoBitinka);
            const bitinkaBuyPrice = await searchBitinkaPrice(bitinkaOrderBook.purchases.NANO, nanoBinance);
    
            if(bitinkaSellPrice > binanceBuyPrice) {
    
                console.log('Valor de venda Bitinka  :', bitinkaSellPrice);
                console.log('Valor de compra Binance :', binanceBuyPrice);
                console.log(`Possível lucro de ${getProfitPercent(bitinkaSellPrice - binanceBuyPrice, binanceBuyPrice)} %`);
    
                if(getProfitPercent(bitinkaSellPrice - binanceBuyPrice, binanceBuyPrice) > Config.candle){
    
                    let sellAmount, buyAmount;
                    if(nanoBitinka * bitinkaSellPrice > binanceBalance.btc){
                        sellAmount = binanceBalance.btc / bitinkaSellPrice;
                        buyAmount = binanceBalance.btc / binanceBuyPrice;
                    } else {
                        sellAmount = nanoBitinka;
                        buyAmount = (nanoBitinka * bitinkaSellPrice) / binanceBuyPrice;
                    }
    
                    if((sellAmount * bitinkaSellPrice) < Config.btinkaMinimumAmount || (buyAmount * binanceBuyPrice) < Config.binanceMinimumAmount){
                        console.log('Saldo abaixo do valor mínimo para operação');
                        return reInit();
                    }
                    
                    sellNanoBitinkaBuyNanoBinance({sellAmount, bitinkaSellPrice, buyAmount, binanceBuyPrice}).then( () => {
    
                        console.log(`vendido ${sellAmount} na bitinka por ${bitinkaSellPrice}, e comprado ${buyAmount} em binance por ${binanceBuyPrice}`);
                        console.log(`Lucro ${getProfitPercent(bitinkaSellPrice - binanceBuyPrice, binanceBuyPrice) - Config.fee} %`);
                        console.log('aguardando nova oportunidade de negocio...');
                        return reInit();
    
                    }).catch(err => {
    
                        console.log('erro executando operação', err);
                        reInit();
    
                    });
                
                } else {
                    console.log(`Operação descartada : ${getProfitPercent(bitinkaSellPrice - binanceBuyPrice, binanceBuyPrice) - Config.fee} %`);
                    return reInit();
                }
    
            } else if (binanceSellPrice > bitinkaBuyPrice) {
    
                console.log('Valor de venda Binance  :', binanceSellPrice);
                console.log('Valor de compra Bitinka :', bitinkaBuyPrice);
                console.log(`Possível lucro de ${getProfitPercent(binanceSellPrice - bitinkaBuyPrice, bitinkaBuyPrice)} %`);
    
                if(getProfitPercent(binanceSellPrice - bitinkaBuyPrice, bitinkaBuyPrice) > Config.candle){
                    const bitinkaBalanceBtc = bitinkaBalance.btc;
                    let sellAmount, buyAmount;
                    if(nanoBinance * binanceSellPrice > bitinkaBalanceBtc){
                        sellAmount = bitinkaBalanceBtc / binanceSellPrice;
                        buyAmount = bitinkaBalanceBtc / bitinkaBuyPrice;
                    } else {
                        sellAmount = nanoBinance;
                        buyAmount = (nanoBinance * binanceSellPrice) / bitinkaBuyPrice;
                    }
    
                    if((sellAmount * binanceSellPrice) < Config.binanceMinimumAmount || (buyAmount * bitinkaBuyPrice) < Config.btinkaMinimumAmount){
                        console.log('Saldo abaixo do valor mínimo para operação');
                        return reInit();
                    }
    
                    sellNanoBinanceBuyNanoBitinka({sellAmount, binanceSellPrice, buyAmount, bitinkaBuyPrice}).then( () => {
    
                        console.log(`vendido ${sellAmount} na binance por ${binanceSellPrice}, e comprado ${buyAmount} em bitinka por ${bitinkaBuyPrice}`);
                        console.log(`Lucro ${getProfitPercent(binanceSellPrice - bitinkaBuyPrice, bitinkaBuyPrice) - Config.fee} %`);
                        console.log('aguardando nova oportunidade de negocio...');
                        return reInit();
    
                    }).catch(err => {
    
                        console.log('erro executando operação', err);
                        reInit();
    
                    });
    
                } else {
                    console.log(`Operação descartada : ${getProfitPercent(binanceSellPrice - bitinkaBuyPrice, bitinkaBuyPrice) - Config.fee} %`);
                    return reInit();
                }
    
            } else {
                console.log('Erro Comparando valores', {binanceSellPrice: binanceSellPrice, bitinkaBuyPrice: bitinkaBuyPrice});
                console.log({bitinkaSellPrice: bitinkaSellPrice, binanceBuyPrice: binanceBuyPrice})
                return reInit();
            }

        }).catch(err => {
            console.log(err.message, err);
            console.log('Ops! Houve um erro. Reiniciando em breve...')
        })
    
    }).catch(err => {
        console.log(err.message, err);
        console.log('Ops! Houve um erro. Reiniciando em breve...');
        reInit();
    });
}

function teste (){
    const totalvalue = 0.00160298;
    const buyAmount = totalvalue / 0.00031640
    postOrderBitinka(buyAmount, 0.00031640, "buy").then(ret =>{
        console.log('comprou', ret);
    }).catch(err => { 
        console.log('erro', err);
     })
}

module.exports = { work, teste };