const Config = require('../../config');
const TradeModel = require('../model/trade');
const { parseNanoAmount, parseBtcAmount, trade, getProfitPercent } = require('../utils');
const {
    getCoinbeneBalance, 
    createOrderCoinbene, 
    getCoinbeneLastPrice,
} = require('../api/coinbene');

const {
    getBinanceBalance, 
    createOrderBinance,
} = require('../api/binance');
const {
    getBinanceLastPrice, 
} = require('../api/binance.v2');

function reInit( time = 5000 ) {
    setTimeout(()=>{
        work();
    }, time); 
}

async function findBinanceBalance(balances) {
    let ret = { btc: 0, nano: 0}
    for(let i in balances){
        if(balances[i].asset === "BTC"){
            ret.btc =  parseFloat(balances[i].free);
        }
        if(balances[i].asset === "NANO"){
            ret.nano =  parseFloat(balances[i].free);
        }
    }
    return ret;
}

async function findCoinbeneBalance(balances) {
    let ret = { btc: 0, nano: 0}
    for(let i in balances){
        if(balances[i].asset === "BTC"){
            ret.btc =  parseFloat(balances[i].available);
        }
        if(balances[i].asset === "NANO"){
            ret.nano =  parseFloat(balances[i].available);
        }
    }
    return ret;
}

async function postOrderCoinbene (qtd, price, orderType) {
    return new Promise(async (result, reject) => {
        const postOrder = async () => {
            let payload = {
                "type": orderType, 
                "price": parseBtcAmount(price),
                "quantity": parseNanoAmount(qtd),
                "symbol": 'nanobtc'
            }
            try {
                const res = await createOrderCoinbene(payload);
                console.log("ordem registadra em Coinbene", res);
                result(res);
            } catch(e) {
                console.log("erro enviando ordem para Coinbene", e);
                console.log("tentando novamente");
                setTimeout(postOrder, 1000);
            }
        }
        console.log("enviando ordem para Coinbene");
        return await postOrder(); 
    })
}

async function postOrderBinance (qtd, price, orderType) {
    return new Promise(async (result, reject) => {
        const postOrder = async () => {
            const payload = {
                symbol: "NANOBTC",
                side: orderType,
                type: "LIMIT",
                timeInForce: "GTC",
                quantity: parseNanoAmount(qtd),
                price: parseBtcAmount(price)
            }
            try {
                const res = await createOrderBinance(payload);
                console.log("ordem registadra em Binance");
                result(res);
            } catch(e) {
                console.log("erro enviando ordem para Binance", e);
                console.log("tentando novamente");
                setTimeout(postOrder, 1000);
            }
        }
        console.log("enviando ordem para binance");
        return await postOrder(); 
    })
}

async function sellConbeneBuyNanoBinance ({sellAmount, xSellPrice, buyAmount, yBuyPrice}) {
   
    return Promise.all([
        postOrderBinance(buyAmount, yBuyPrice, "BUY").catch(err => { throw err }),
        postOrderCoinbene(sellAmount, xSellPrice, "sell-limit").catch(err => { throw err })
    ])
    .then(() => {

        const newTrade = {
            symbol           : 'NANOBTC',
            xName            : 'Coinbene',
            xAmount          : sellAmount,
            xPrice           : xSellPrice,
            xAction          : 'SELL',
            yName            : 'Binance',
            yAmount          : buyAmount,
            yPrice           : yBuyPrice,
            yAction          : 'BUY',
            profit           : buyAmount - sellAmount,
            profitPercetage  : getProfitPercent(xSellPrice - yBuyPrice, yBuyPrice) - Config.fee
        }
        const trade = new TradeModel(newTrade);
        trade.save();

        return true;

    }).catch(err => {
        console.log('erro enviando ordem', err);
        throw err;
    });
            
}

async function sellNanoBinanceBuyNanoCoinbene ({sellAmount, xSellPrice, buyAmount, yBuyPrice}) {
 
    return Promise.all([
        postOrderBinance(sellAmount, xSellPrice, "SELL").catch(err => { throw err }),
        postOrderCoinbene(buyAmount, yBuyPrice, "buy-limit").catch(err => { throw err })
    ])
    .then(() => {

        const newTrade = {
            symbol           : 'NANOBTC',
            xName            : 'Coinbene',
            xAmount          : buyAmount,
            xPrice           : yBuyPrice,
            xAction          : 'BUY',
            yName            : 'Binance',
            yAmount          : sellAmount,
            yPrice           : xSellPrice,
            yAction          : 'SELL',
            profit           : buyAmount - sellAmount,
            profitPercetage  : getProfitPercent(xSellPrice - yBuyPrice, yBuyPrice) - Config.fee
        }
        const trade = new TradeModel(newTrade);
        trade.save();

        return true;

    }).catch(err => {
        console.log('erro enviando ordem', err);
        throw err;
    });
}

async function work () {
    console.log("  ========================= Analizando Informações =====================================  ");

    const err = err => { throw err };
    Promise.all([
        getBinanceBalance().catch(err),
        getBinanceLastPrice('NANO/BTC').catch(err),
        getCoinbeneBalance().catch(err),
        getCoinbeneLastPrice('nanobtc').catch(err)
    ])
    .then(async (response) => {

        let binanceBalance = await findBinanceBalance(response[0].balances);
        const binancePrice = response[1];

        let coinbeneBalance = await findCoinbeneBalance(response[2].balance);
        const coinbenePrice = response[3];

        console.log('Saldo Binance', binanceBalance);
        console.log('Saldo Coinbene', coinbeneBalance);

        if(coinbeneBalance.nano > 100){
            coinbeneBalance.nano = 100;
        }
        if(binanceBalance.nano > 100){
            binanceBalance.nano = 100;
        }

        Promise.all([
            trade ({
                xname : 'Coinbene', 
                xBuyPrice: coinbenePrice,
                xSellPrice : coinbenePrice, 
                xBalance : coinbeneBalance.nano,
                xMinimumAmount: Config.coinbeneMinimumAmount,

                yname : 'Binance',
                yBuyPrice : binancePrice,
                yBalance : binanceBalance.btc,
                yMinimumAmount: Config.binanceMinimumAmount,
                
                action: sellConbeneBuyNanoBinance
            }).catch(async err => { throw err }),

            trade ({
                xname : 'Binance', 
                xBuyPrice: binancePrice,
                xSellPrice : binancePrice,
                xBalance : binanceBalance.nano,
                xMinimumAmount: Config.binanceMinimumAmount,

                yname : 'Coinbene',
                yBuyPrice : coinbenePrice,
                yBalance : coinbeneBalance.btc,
                yMinimumAmount: Config.coinbeneMinimumAmount,

                action: sellNanoBinanceBuyNanoCoinbene
            }).catch(err => { throw err }),

        ]).then(data => {
            reInit()
            console.log('aguardando nova oportunidade de negocio...');
        }).catch(err => { throw err })

    }).catch(err => {
        console.log(err.message, err);
        console.log('Ops! Houve um erro. Reiniciando em breve...');
        return reInit();
    })
}

module.exports = { work };