const Config = require('../../config');
const Request = require('request-promise-native');
// const CryptoJS = require('crypto-js');
// const { getDate } = require('../utils');
const ccxt = require ('ccxt');

const huobi = new ccxt.huobipro ({
    apiKey: Config.huobiApiKei,
    secret: Config.huobiSecretKey
});


async function getHuobiOrderBook(symbol){
    try {
        const options = {
            uri: `${Config.huobiBaseUrl}market/depth`,
            qs: {
                symbol: symbol,
                type: "step1"
            },
            json: true
        };
        const res = await Request(options);
        if(res.status === 'error'){
            throw res;
        }
        return res.tick;
    } catch (e) {
        throw { message: "Error getting huobi order book", error: e }
    }
}

async function getHuobiBalance(){
    try {
        return await huobi.fetchBalance();
    } catch (e) {
        throw { message: "Error getting huobi account balance", error: e }
    }
}

async function createOrderHuobi({symbol, side, type, price, quantity}){
    try {
        const res = await huobi.createOrder (symbol, type, side, quantity, price);
        console.log('order res', res)
        if(res.status === 'error'){
            throw res;
        }
        return res;

    } catch (e) {
        throw { message: "Error creating order huobi", error: e }
    }
}

async function searchHuobiPrice (array, total) {
    let qtd = 0;
    for(let i in array){
        const item = array[i];
        qtd = qtd + parseFloat(item[1]);
        if(qtd >= total){
            return parseFloat(item[0].toString());
        }
    }
}

module.exports = {getHuobiOrderBook, getHuobiBalance, createOrderHuobi, searchHuobiPrice};