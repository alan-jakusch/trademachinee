const Config = require('../../config');
const ccxt = require ('ccxt');

const kucoin = new ccxt.kucoin2  ({
    apiKey: Config.kucoinApiKei,
    secret: Config.kucoinSecretKey,
    password : Config.kucoinPassword
});


async function getKucoinOrderBook(symbol = 'NANO/BTC'){
    try {
        return await kucoin.fetchOrderBook (symbol);
    } catch (e) {
        throw { message: "Error getting kucoin order book", error: e }
    }
}

async function getKucoinLastPrice(symbol = 'NANO/BTC'){
    try {
        const res = await kucoin.fetchTicker(symbol);
        // console.log('ticker', res);
        return res.info.last;
    } catch (e) {
        // console.log('e', e)
        throw { message: "Error getting kucoin order book", error: e }
    }
}

async function getKucoinBalance(){
    try {
        const res = await kucoin.fetchBalance();
        // console.log('res', res)
        return res
    } catch (e) {
        throw { message: "Error getting kucoin account balance", error: e }
    }
}

async function createOrderKucoin({symbol, side, type, price, quantity}){
    try {
        const res = await kucoin.createOrder (symbol, type, side, quantity, price);
        return res;

    } catch (e) {
        throw { message: "Error creating order kucoin", error: e }
    }
}

async function searchKucoinPrice (array, total) {
    let qtd = 0;
    for(let i in array){
        const item = array[i];
        qtd = qtd + parseFloat(item[1]);
        if(qtd >= total){
            return parseFloat(item[0].toString());
        }
    }
}

module.exports = {getKucoinOrderBook, getKucoinBalance, createOrderKucoin, searchKucoinPrice, getKucoinLastPrice};