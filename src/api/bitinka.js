const Config = require('../../config');
const Request = require('request-promise-native');

async function getBitinkaOrderBook(symbol){
    try {
        console.log('chamando getBitinkaOrderBook');
        const options = {
            uri: `${Config.bitinkaBaseUrl}order_book/${symbol}`,
            qs: {
                format: 'json',
            },
            json: true
        };
        const data = await Request(options);
        if(!data.orders.purchases || !data.orders.sales){
            throw { message: "Error getting bitinka order book1" };
        }
        return data.orders;
    } catch (e) {
        throw { message: "Error getting bitinka order book2", error: e }
    }
}

async function getBitinkaBalance(){
    try {
        console.log('chamando getBitinkaBalance');
        const options = {
            method: 'POST',
            body: {
                key : Config.bitinkaApiKey,
                secret: Config.bitinkaSecretKey
            },
            uri: `${Config.bitinkaBaseUrl}get_balance/format/json`,
            json: true
        };
        return await Request(options);
    } catch (e) {
        throw { message: "Error getting bitinka balance", error: e }
    }
}

async function createOrderBitinka({typeOrder, price, investement, firstCurrency, secondCurrency}){
    try {
        const options = {
            method: 'POST',
            body: {
                key : Config.bitinkaApiKey,
                secret: Config.bitinkaSecretKey,
                new: {
                    typeOrder : typeOrder, 
                    price : price.toString(),
                    investement : investement.toString(), 
                    firstCurrency : firstCurrency, 
                    secondCurrency : secondCurrency, 
                    trade : 1 
                }
            },
            uri: `${Config.bitinkaBaseUrl}create_order/format/json`,
            json: true
        };
        return await Request(options);
    } catch (e) {
        if(e.statusCode === 404){
            console.log('erro 404 criando ordem bitinka')
            return true
        }
        throw { message: "Error creating order bitinka", error: e }
    }
}

async function getOpenOrdersBitinka({firstCurrency, secondCurrency}){
    try {
        const options = {
            method: 'POST',
            body: {
                key : Config.bitinkaApiKey,
                secret: Config.bitinkaSecretKey,
                firstCurrency : firstCurrency, 
                secondCurrency : secondCurrency, 
                trade: 1
            },
            uri: `${Config.bitinkaBaseUrl}orders_user/format/json`,
            json: true
        };
        return await Request(options);
    } catch (e) {
        throw { message: "Error getting open orders bitinka", error: e }
    }
}

async function searchBitinkaPrice(array, total) {
    let qtd = 0;
    for(let i in array){
        const item = array[i];
        qtd = qtd + item.Amount;
        if(qtd >= total){
            return item.Price;
        }
    }
}

module.exports = {getBitinkaOrderBook, getBitinkaBalance, createOrderBitinka, getOpenOrdersBitinka, searchBitinkaPrice };