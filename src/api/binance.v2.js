const Config = require('../../config');
const Request = require('request-promise-native');
const CryptoJS = require('crypto-js');
const { getDate } = require('../utils');

const ccxt = require ('ccxt');

const binance = new ccxt.binance  ({
    apiKey: Config.binanceApiKei,
    secret: Config.binanceSecretKey,
    // password : Config.binancePassword
});

async function getBinanceLastPrice(symbol = 'NANO/BTC'){
    try {
        const res = await binance.fetchTicker(symbol);
        return res.info.lastPrice;
    } catch (e) {
        // console.log('e', e)
        throw { message: "Error getting binance order book", error: e }
    }
}

async function getBinanceOrderBook(symbol){
    try {
        const options = {
            uri: `${Config.biananceBaseUrl}api/v1/depth`,
            qs: {
                symbol: symbol,
            },
            json: true
        };
        return await Request(options);
    } catch (e) {
        throw { message: "Error getting binance order book", error: e }
    }
}

async function getBinanceBalance(){
    try {
        const qs= `timestamp=${getDate()}`;
        const signature = CryptoJS.HmacSHA256(qs, Config.binanceSecretKey).toString(CryptoJS.enc.Hex);
        const options = {
            headers: {
                'X-MBX-APIKEY': Config.binanceApiKey
            },
            uri: `${Config.biananceBaseUrl}api/v3/account?${qs}&signature=${signature}`,
            json: true
        };
        return await Request(options);
    } catch (e) {
        throw { message: "Error getting binance balance", error: e }
    }
}

async function createOrderBinance({symbol, side, type, price, quantity}){
    try {
        const qs= `symbol=${symbol}&side=${side}&type=${type}&price=${price}&timeInForce=GTC&quantity=${quantity}&timestamp=${getDate()}`;
        const signature = CryptoJS.HmacSHA256(qs, Config.binanceSecretKey).toString(CryptoJS.enc.Hex);
        const options = {
            method: 'POST',
            headers: {
                'X-MBX-APIKEY': Config.binanceApiKey
            },
            uri: `${Config.biananceBaseUrl}api/v3/order?${qs}&signature=${signature}`,
            json: true
        };
        return await Request(options);
    } catch (e) {
        throw { message: "Error creating order binance", error: e }
    }
}

async function getOpenOrdersBinance(symbol){
    try {
        const qs= `symbol=${symbol}&timestamp=${getDate()}`;
        const signature = CryptoJS.HmacSHA256(qs, Config.binanceSecretKey).toString(CryptoJS.enc.Hex);
        const options = {
            headers: {
                'X-MBX-APIKEY': Config.binanceApiKey
            },
            uri: `${Config.biananceBaseUrl}api/v3/openOrders?${qs}&signature=${signature}`,
            json: true
        };
        return await Request(options);
    } catch (e) {
        throw { message: "Error geting open orders binance", error: e }
    }
}

async function searchBinancePrice (array, total) {
    let qtd = 0;
    for(let i in array){
        const item = array[i];
        qtd = qtd + parseFloat(item[1]);
        if(qtd >= total){
            return parseFloat(item[0].toString());
        }
    }
}

module.exports = {getBinanceOrderBook, getBinanceBalance, createOrderBinance, getOpenOrdersBinance, searchBinancePrice, getBinanceLastPrice};