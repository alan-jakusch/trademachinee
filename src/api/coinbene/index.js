const rp = require('request-promise-native');
const md5 = require('js-md5');
const Config = require('../../../config');

const HEADER_DICT = {
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Trident/7.0; rv:11.0) like Gecko",
    "Content-Type": "application/json;charset=utf-8",
    "Connection": "keep-alive"
}

function sign(data) {
    let keys = Object.keys(data);
    keys.sort();

    let message = "";
    for (let i = 0; i < keys.length; i++) {
        if (i > 0) {
            message += '&';
        }
        message += keys[i] + '=' + data[keys[i]];
    }

    let hash = md5.create();
    hash.update(message.toUpperCase());
    return hash.hex();
}

function httpPostSign(url, dic) {
    let mysign = sign(dic);
    delete dic['secret'];
    dic['sign'] = mysign;

    return httpRequest(url, dic);
}

async function httpRequest(url, data) {
    let response;
    if (data === undefined) {
        response = await rp(url);
    } else {
        let option = {
            method: 'POST',
            uri: url,
            body: data,
            header: HEADER_DICT,
            json: true
        }
        response = JSON.stringify(await rp(option));
    }
    return response;
}

async function getCoinbeneBalance () {
    try {
        const config = {
            apiid: Config.coinbeneApiKey,
            secret: Config.coinbeneSecretKey,
            timestamp: new Date().getTime(),
            account: 'exchange',
        }
        url = Config.coinbeneTradeUrl + "balance";
        let res = JSON.parse(await httpPostSign(url, config));
        return res;
    } catch (e) {
        console.log('error getCoinbeneBalance res', e);
        throw { message: "Error getting coinbene account balance", error: e }
    }
}

async function getCoinbeneLastPrice (symbol) {
    try {
        let url = Config.coinbeneMarketUrl + "ticker?symbol=" + symbol;
        const res = JSON.parse(await httpRequest(url));
        return res.ticker[0].last;
    } catch (e) {
        console.log('error getCoinbeneLastPrice', e);
        throw { message: "Error getting coinbene order book", error: e }
    }
}

async function createOrderCoinbene (payload) {
    try {
        payload.apiid = Config.coinbeneApiKey;
        payload.secret = Config.coinbeneSecretKey;
        payload.timestamp = new Date().getTime();
        url = Config.coinbeneTradeUrl + "order/place";
        const res = JSON.parse(await httpPostSign(url, payload));
        console.log('crate order coinbene');
        return res;
    } catch (e) {
        console.log('error crate order coinbene', e);
        throw { message: "Error creating order coinbene", error: e }
    }
}

module.exports = {createOrderCoinbene, getCoinbeneLastPrice, getCoinbeneBalance};