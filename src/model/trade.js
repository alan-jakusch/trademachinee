const mongoose = require('mongoose');
const Bluebird = require('bluebird');

mongoose.Promise = Bluebird;

const Schema = mongoose.Schema;

function tradeModel () {

    const tradeSchema = new Schema({
        date             : { type: Date, default: Date.now },
        symbol           : { type: String },
        xName            : { type: String },
        xAmount          : { type: Number },
        xPrice           : { type: Number },
        xAction          : { type: String },
        yName            : { type: String },
        yAmount          : { type: Number },
        yPrice           : { type: Number },
        yAction          : { type: String },
        profit           : { type: Number },
        profitPercetage  : { type: String } 
    });

    return mongoose.model('trade', tradeSchema);
  
}

module.exports = tradeModel();