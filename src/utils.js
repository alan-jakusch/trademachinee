const Config = require('../config');

function getDate() {
    return Date.now() - 2000;
}
function to(promise) {
    return promise.then(data => {
        return [null, data];
    })
    .catch(err => [err]);
}

function parseNanoAmount(value){
    const v = value.toString().split('.');
    return parseFloat(`${v[0]}.${v[1].slice(0,2)}`);
}

function parseBtcAmount(value){
    const v = value.toString().split('.');
    return parseFloat(`${v[0]}.${v[1].slice(0,8)}`);
}

function getProfitPercent (profit, valor) {
    const x = (profit * 100) / valor;
    return x
}

async function getOperationValues (xBalance, xBuyPrice, xSellPrice, yBalance ) {
    let sellAmount, buyAmount;
    if(xBalance * xSellPrice > yBalance){
        sellAmount = yBalance / xSellPrice;
        buyAmount = yBalance / xBuyPrice;
    } else {
        sellAmount = xBalance;
        buyAmount = (xBalance * xSellPrice) / xBuyPrice;
    }

    return {
        sellAmount,
        buyAmount
    }
}

async function trade (data) {

    let { xname, yname, xSellPrice, xBuyPrice, yBuyPrice, xBalance, yBalance, action, xMinimunAmount, yMinimumAmount } = data;
    
    yBuyPrice = (parseFloat(yBuyPrice) + 0.0000001).toString();
    xSellPrice = (parseFloat(xSellPrice) - 0.0000001).toString();

    if (getProfitPercent(xSellPrice - yBuyPrice, yBuyPrice) - Config.fee > Config.minProfit) {

        console.log(`Valor de compra ${yname} :`, yBuyPrice);
        console.log(`Valor de venda ${xname}  :`, xSellPrice);
        console.log(`Lucro de ${getProfitPercent(xSellPrice - yBuyPrice, yBuyPrice) - Config.fee} %`);


        const { sellAmount, buyAmount } = await getOperationValues( xBalance, xBuyPrice, xSellPrice, yBalance );
        if(((sellAmount * xSellPrice) < xMinimunAmount || (buyAmount * yBuyPrice) < yMinimumAmount) || (sellAmount < Config.minimunNanoAmount || buyAmount < Config.minimunNanoAmount)){
            console.log('Saldo abaixo do valor mínimo para operação');
            return false;
        }

        // console.log('{sellAmount, xSellPrice, buyAmount, yBuyPrice}', {sellAmount, xSellPrice, buyAmount, yBuyPrice})
        // return true

        action({sellAmount, xSellPrice, buyAmount, yBuyPrice}).then( () => {
            console.log(`vendido ${sellAmount} na ${xname} por ${xSellPrice}, e comprado ${buyAmount} em ${yname}  por ${yBuyPrice}`);
            console.log(`Lucro ${getProfitPercent(xSellPrice - yBuyPrice, yBuyPrice) - Config.fee} %`);
            return true;
        }).catch(err => { throw err });

    } else {
        console.log(`Valor de venda ${xname} : ${xSellPrice}\n Valor de compra ${yname} : ${yBuyPrice}\n Prejuizo : ${getProfitPercent(xSellPrice - yBuyPrice, yBuyPrice) - Config.fee} %` );
        return false;
    }
}

module.exports = { getDate, to, parseBtcAmount, parseNanoAmount, trade, getProfitPercent }