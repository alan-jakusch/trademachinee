var assert = require('assert');
const {getBitinkaOrderBook, getBitinkaBalance, createOrderBitinka, getOpenOrdersBitinka} = require('../src/api/bitinka')

// describe('bitinka api', function() {
// 	describe('Test api', function() {

// 		it('get bitinka order book',function(done) {
//             this.timeout(60000);
//             getBitinkaOrderBook('NANO_BTC').then(book => {
//                 assert(book.sales.NANO.length > 0 && book.purchases.NANO.length > 0)
// 				done();
//             }).catch(err => {
//                 done(e)
//             })
//         })
        
//         it('get bitinka balance',function(done) {
//             this.timeout(60000);
//             getBitinkaBalance().then(data => {
//                 assert(!!data)
// 				done();
//             }).catch(err => {
//                 done(err)
//             })
//         })
        
//         it('create order bitinka',function(done) {
//             this.timeout(60000);
//             const opt = {typeOrder: "buy", price: 0.00033900, investement: 5.1, firstCurrency: "NANO", secondCurrency: "BTC"}
//             createOrderBitinka(opt).then(data => {
//                 assert(!!data)
// 				done();
//             }).catch(err => {
//                 done(err)
//             })
//         })
        
//         it('get open orders bitinka',function(done) {
//             this.timeout(60000);
//             getOpenOrdersBitinka({firstCurrency: "NANO", secondCurrency: "BTC"}).then(data => {
//                 assert(!!data)
// 				done();
//             }).catch(err => {
//                 done(e)
//             })
// 		})
// 	})

// });